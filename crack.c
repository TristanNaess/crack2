#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "md5.h"
#include <unistd.h>

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char * guess_hash;
    guess_hash = md5(guess, strlen(guess));
    // Compare the two hashes

    if (!strcmp(hash, guess_hash))
    {
        free(guess_hash);
        return 1;
    }
    free(guess_hash);
    return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    *size = 0;
    // open file
    FILE * fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Error opening file: %s\n", filename);
        exit(1);
    }

    // build struct for size
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1)
    {
        printf("Error getting info on %s\n.", filename);
        exit(1);
    }
    
    int len = info.st_size;

    // allocate memory
    char * text = (char *)malloc(len * sizeof(char));
    fread(text, sizeof(char), len, fp);
    fclose(fp);

    /* check file reading.
    for (int i = 0; i < info.st_size; i++)
    {
        printf("%c", text[i]);
    }
    */

    // replace \n with \0
    for (int i = 0; i < len; i++)
    {
        if (text[i] == '\n')
        {
            text[i] = '\0';
            (*size)++;
        }
    }

    char ** lines = (char **)malloc(*size * sizeof(char *));
    lines[0] = text;
    int j = 1;
    for(int i = 1; i < len - 1; i++)
    {
        if (text[i] == '\0')
        {
            lines[j] = &text[i+1];
            //printf("%d: %s\n", j, lines[j]);
            j++;
        }
    }

    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE * hash_file = fopen(argv[1], "r");
    if (!hash_file)
    {
        printf("Error opening hash file: %s", argv[1]);
    }
    // For each hash, try every entry in the dictionary
    char hash[33];
    char line[34];
    while (fgets(line, 34, hash_file))
    {
        line[strlen(line)-1] = '\0';
        strcpy(hash, line);

        printf("Cracking %s\n", hash);
        //sleep(1);
        for (int i = 0; i < dlen; i++)
        {
            if (tryguess(hash, dict[i]))
            {
                printf("%s: %s\n\n", hash, dict[i]);
                break;
            }
        }
    }
    free(dict);
}
